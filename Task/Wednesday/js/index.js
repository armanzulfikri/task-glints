
var posts = [
        {
            id : 1,
            title : "Salary Income",
            type : "income",
            total : 5000000
        }
    ];


getData();

function submitHandler(){
    // console.log("Click already worked.");
    var postValue = document.getElementById('post-title').value;
    var postType = document.getElementById('post-type').value;
    var postTotal = document.getElementById('post-total').value;
    
    addData(postValue,postType,postTotal);
    getData();
    document.getElementById('post-title').value = "";

    return false;
}
function getData(){
    var postData = document.getElementById('post-data');
    postData.innerHTML = "";
    
    for(let i=0; i<posts.length; i++){
        let postHTML = `
            <p>${posts[i].id}. ${posts[i].title} , type : ${posts[i].type}, total : ${posts[i].total}</p>
        `

        postData.insertAdjacentHTML('beforeend',postHTML);
    }
    
}
function addData(title,type,total){
    var postObject = createObject(title,type,total);
    posts.push(postObject);
    console.log(`"${title}" has been inserted!`);

}

function createObject(title,type,total){
    var temp = {
        id : generateId() + 1,
        title : title,
        type : type,
        total : total
    }
    return temp
}

function generateId(){
    var postsLength = posts.length;
    var id = posts[postsLength - 1].id;
    return id
}