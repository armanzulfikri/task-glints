// Class Army
 class Army {
    constructor(name,type,level){
        this.name =  name;
        this.type = type;
        this.level = level || 1;
    }

    training(){
        if (this.level <= 10){
            let randomLevel = Math.ceil(Math.random()* 2) ;
            this.level = this.level + randomLevel;
        }else {
            console.log("Levelmax");
        } 
    }

    talk(){
        console.log(`My Name ${this.name} my type ${this.type} iam level ${this.level}`);
    }
 }
 class Knight extends Army{
     constructor(name){
         super(name,"Knight", 1);
         this.name = name;
     }
     talkKnight(){
         super.talk();
         console.log("I will be crush all enemies");

     }
 }
 
 class Spear extends Army{
    constructor(name){
        super(name,"Spear", 1);
        this.name = name;
    }
    talkSpear(){
        super.talk();
        console.log("Heroes never die....");

    }
}

class Archer extends Army{
    constructor(name){
        super(name,"Archer", 1);
        this.name = name;
    }
    talkArcher(){
        super.talk();
        console.log("my arrow never miss");

    }
}

module.exports = {
    Knight : Knight,
    Spear : Spear,
    Archer : Archer
}

